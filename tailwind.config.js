/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        green_ligth: "#30BF8A",
        green_middle: "#00A892",
        green_dark: "#009090",
      },
      fontFamily: {
        headings: ["Oswald", "sans-serif"],
        paragraph: ["Lato", "sans-serif"],
      },
    },
    plugins: [],
  },
};
