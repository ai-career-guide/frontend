import { Outlet, useNavigate } from "react-router-dom";
import Logo from "../../assets/Logo.png";
import { useDispatch } from "react-redux";
import {
  clearAnswers,
  setJobTitles,
  setQuestions,
  setRoadmap,
  setVacancies,
} from "../../store/slices/QuestionsSlice";
import { closeVacancies } from "../../store/slices/VacanciesSlice";

export const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const styles = {
    height: "10%",
  };

  return (
    <div className="w-screen h-screen">
      <div style={styles} className="flex items-center">
        <img
          className="object-contain w-40 md:w-55 p-2 hover:cursor-pointer"
          onClick={() => {
            dispatch(setQuestions([]));
            dispatch(setJobTitles([]));
            dispatch(setVacancies([]));
            dispatch(setRoadmap([]));
            dispatch(clearAnswers())
            dispatch(closeVacancies())
            navigate("/", { replace: true });
          }}
          src={Logo}
          alt="logo"
        />
      </div>
      <Outlet />
    </div>
  );
};
