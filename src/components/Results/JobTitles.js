import { useSelector } from "react-redux";
import { JobTitle } from "../../components/Results/JobTitle";

export const JobTitles = () => {

  const job_titles = useSelector((state) => state.questions.job_titles);
  return (
    <div className="flex flex-col items-center w-11/12 sm:w-4/12 h-5/6 bg-gradient-to-b to-[#00A892] from-[#30BF8A] py-4 rounded-xl mt-10">
        {" "}
        <h1 className="text-4xl font-headings"> Job titles </h1>
        <hr className="border-green_dark border-2 w-full my-2" />
        {job_titles.map((job_title, index) => {
          return (
            <JobTitle
              key={index}
              job_title={job_title}
            />
          );
        })}

    </div>
  );
};
