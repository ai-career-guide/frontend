import { useDispatch } from "react-redux";
import { openVacancies } from "../../store/slices/VacanciesSlice";
import { URL } from "../Constants";
import axios from "axios";
import { stopLoading, setLoading } from "../../store/slices/AnimationSlice";
import { setRoadmap, setVacancies } from "../../store/slices/QuestionsSlice";

export const JobTitle = (props) => {
  const dispatch = useDispatch();

  return (
    <div
      onClick={() => {
        dispatch(setLoading());
        dispatch(openVacancies());
        const data = {};
        data["job_title"] = props.job_title;
        axios.post(`${URL}/roadmap_vacancies`, data).then((response) => {
          console.log(response.data);
          dispatch(setVacancies(response.data.response["vacancies"]));
          dispatch(setRoadmap(response.data.response["roadmap"]));
          dispatch(stopLoading());
        });
      }}
      className="flex justify-center text-start px-6 py-2 w-11/12 text-2xl my-2 bg-green_ligth rounded-xl hover:cursor-pointer hover:bg-green_dark font-paragraph"
    >
      {props.job_title}
    </div>
  );
};
