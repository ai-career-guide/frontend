import { useEffect, useRef, useState } from "react";
import ReactFlow, { Controls, Background, Position } from "reactflow";
import "reactflow/dist/style.css";
import { useSelector } from "react-redux";
import { LoadingAnimation } from "../LoadingAnimation";

export const Roadmap = () => {
  const parentRef = useRef(null);

  const loading_animation = useSelector((state) => state.animation.isLoading);

  const roadmap = useSelector((state) => state.questions.roadmap);

  const box_offset = 120;

  const x_offset_2 = 300;
  const y_offset_4 = 80;

  const y_interval_offset = 300;

  const directions4 = [
    [-x_offset_2 - box_offset, -y_offset_4],
    [-x_offset_2 - box_offset, y_offset_4],
    [x_offset_2 - box_offset, -y_offset_4],
    [x_offset_2 - box_offset, y_offset_4],
  ];

  const directions2 = [-x_offset_2 - box_offset, x_offset_2 - box_offset];

  const node_2_left_nested = {
    sourcePosition: Position.Right,
    targetPosition: Position.Right,
  };

  const node_2_right_nested = {
    sourcePosition: Position.Left,
    targetPosition: Position.Left,
  };

  const node_2_middle = {
    sourcePosition: Position.Top,
    targetPosition: Position.Bottom,
  };

  useEffect(() => {
    if (Object.keys(roadmap).length !== 0) {
      const nodes = [];
      const edges = [];
      for (let i = 0; i < Object.keys(roadmap).length; i++) {
        nodes.push({
          id: `${i}`,
          data: { label: Object.keys(roadmap)[i] },
          position: {
            x: parentRef.current.offsetWidth / 2 - box_offset,
            y: 50 + i * y_interval_offset,
          },
          style: {
            width: box_offset * 2,
          },
          ...node_2_middle,
        });
        const el_number = Math.min(roadmap[Object.keys(roadmap)[i]].length, 4);
        for (let j = 0; j < el_number; j++) {
          nodes.push({
            id: `${i}-${j}`,
            data: { label: roadmap[Object.keys(roadmap)[i]][j] },
            position: {
              x:
                el_number <= 2
                  ? parentRef.current.offsetWidth / 2 + directions2[j]
                  : parentRef.current.offsetWidth / 2 + directions4[j][0],
              y:
                el_number <= 2
                  ? 50 + i * y_interval_offset
                  : 50 + i * y_interval_offset + directions4[j][1],
            },
            style: {
              width: box_offset * 2,
            },
            ...(j < el_number / 2 ? node_2_left_nested : node_2_right_nested),
          });
        }
        if (el_number === 2) {
          edges.push({
            id: `${i}-0-${i}-1`,
            source: `${i}-0`,
            target: `${i}-1`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
        }
        if (el_number === 3) {
          edges.push({
            id: `${i}-0-${i}-1`,
            source: `${i}-0`,
            target: `${i}-2`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
          edges.push({
            id: `${i}-0-${i}-2`,
            source: `${i}-1`,
            target: `${i}`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
        }
        if (el_number === 4) {
          edges.push({
            id: `${i}-0-${i}-1`,
            source: `${i}-0`,
            target: `${i}-2`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
          edges.push({
            id: `${i}-0-${i}-2`,
            source: `${i}-1`,
            target: `${i}-3`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
        }
        if (i === 0 && el_number !== 2) {
          edges.push({
            id: `${i}-${i}-0`,
            source: `${i}`,
            target: `${i}-0`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
        }
        if (i === Object.keys(roadmap).length - 1 && el_number > 2) {
          console.log("entered");
          edges.push({
            id: `${i}-${i}-${el_number - 1}`,
            source: `${i}-1`,
            target: `${i}`,
            type: "smoothstep",
            style: {
              stroke: "#FFFFFF",
            },
          });
        }
        edges.push({
          id: `${i}-0`,
          source: `${i}`,
          target: `${i + 1}`,
          style: {
            stroke: "#FFFFFF",
          },
        });
      }
      setNodes(nodes);
      setEdges(edges);
    }
  }, [parentRef, roadmap]);

  const [nodes, setNodes] = useState([]);
  const [edges, setEdges] = useState([]);

  return (
    <div className="flex flex-col  items-center w-11/12 sm:w-7/12 h-5/6 bg-gradient-to-b to-[#00A892] from-[#30BF8A] mt-10 py-4 rounded-xl">
      <h1 className="text-4xl font-headings"> Roadmap </h1>
      <hr className="border-green_dark border-2 w-full my-2" />
      {loading_animation ? (
        <LoadingAnimation />
      ) : (
        <div
          ref={parentRef}
          className="flex justify-center items-center w-full h-full"
        >
          {Object.keys(roadmap).length === 0 ? (
            <h1 className="text-xl font-paragraph">Select a job title to see its roadmap</h1>
          ) : (
            <ReactFlow nodes={nodes} edges={edges} fitView>
              <Background />
              <Controls />
            </ReactFlow>
          )}
        </div>
      )}
    </div>
  );
};
