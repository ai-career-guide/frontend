export const Vacancy = (props) => {
    return (
      <div className="flex flex-col justify-center text-start  py-2 w-11/12 text-2xl my-2 bg-green_ligth rounded-xl">
        <div className="flex flex-row justify-between items-center px-6">
          {props.title}
          <a href={props.link} className="text-[#215F72]" target="_blank">
            Link
          </a>
        </div>
        <hr className="border-green_dark border w-full my-2" />
        <div className="flex flex-row justify-between items-center px-6 py-2 text-lg">
          {props.location}

          {props.company}
        </div>
      </div>
    );
}