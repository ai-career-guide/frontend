import LeftArrow from "../../assets/LeftArrow.png";
import { closeVacancies } from "../../store/slices/VacanciesSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { LoadingAnimation } from "../LoadingAnimation";
import { Vacancy } from "./Vacancy";

export const Vacancies = () => {
  const dispatch = useDispatch();

  const animation_loading = useSelector((state) => state.animation.isLoading);

  const vacancies = useSelector((state) => state.questions.vacancies);

  return (
    <div className="flex flex-col items-center w-11/12 sm:w-4/12 h-5/6 bg-gradient-to-b to-[#00A892] from-[#30BF8A] py-4 rounded-xl mt-10 overflow-auto no-scrollbar">
      <div className="flex flex-row justify-center items-center h-fit w-full">
        <h1 className="text-4xl text-center pl-10 w-5/6 font-headings"> Vacancies </h1>
        <button
          onClick={() => {
            dispatch(closeVacancies());
          }}
          className="flex justify-center items-center self-end text-2xl bg-green_ligth w-15 h-10 px-2 rounded-lg"
        >
          <img src={LeftArrow} alt="Left arrow" />
        </button>
      </div>
      <hr className="border-green_dark border-2 w-full my-2" />
      {animation_loading ? (
        <LoadingAnimation />
      ) : (
        <div className="w-full flex flex-col justify-center items-center font-paragraph">
          {vacancies.map((vacancy, index) => {
            return (
              <Vacancy
                key={index}
                title={vacancy["title"]}
                location={vacancy["location"]}
                link={vacancy["link"]}
                company={vacancy["company"]}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};
