import { combineReducers, configureStore } from "@reduxjs/toolkit";
import questionsReducer from "./slices/QuestionsSlice";
import vacanciesReducer from "./slices/VacanciesSlice";
import animationReducer from "./slices/AnimationSlice";

const rootReducer = combineReducers({
    questions: questionsReducer,
    vacancies: vacanciesReducer,
    animation: animationReducer

});

const store = configureStore({
    reducer: rootReducer
});

export default store;