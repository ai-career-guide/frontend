import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isOpen: false
};

const vacanciesSlice = createSlice({
    name: "vacanciesSlice",
    initialState,
    reducers: {
        openVacancies: (state) => {
            state.isOpen = true;
        },
        closeVacancies: (state) => {
            state.isOpen = false;

        }
    }
});

export const { openVacancies, closeVacancies } = vacanciesSlice.actions;

export default vacanciesSlice.reducer;