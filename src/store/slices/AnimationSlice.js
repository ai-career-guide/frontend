import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
};

const animationSlice = createSlice({
  name: "animationSlice",
  initialState,
  reducers: {
    setLoading: (state) => {
      state.isLoading = true;
    },
    stopLoading: (state) => {
      state.isLoading = false;
    },
  },
});

export const { setLoading, stopLoading } =
  animationSlice.actions;

export default animationSlice.reducer;
