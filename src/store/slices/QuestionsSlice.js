import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  answers: [],
  questions: [],
  job_titles: [],
  vacancies: [],
  roadmap: [],
};

const questionsSlice = createSlice({
  name: "questionsSlice",
  initialState,
  reducers: {
    addAnswer: (state, action) => {
      const id = action.payload.id;
      const key = action.payload.key;
      const value = action.payload.value;
      state.answers[id] = [key, value];
    },
    clearAnswers(state) {
      state.answers = []
    },
    setQuestions: (state, action) => {
      state.questions = action.payload;
    },
    setJobTitles: (state, action) => {
      state.job_titles = action.payload;
    },
    setVacancies: (state, action) => {
      state.vacancies = action.payload;
    },
    setRoadmap: (state, action) => {
      state.roadmap = action.payload;
    },
  },
});

export const {
  addAnswer,
  setQuestions,
  setJobTitles,
  setVacancies,
  setRoadmap,
  clearAnswers
} = questionsSlice.actions;

export default questionsSlice.reducer;
