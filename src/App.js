import { Route, Routes } from "react-router-dom";
import "./App.css";
import { Header } from "./components/Header/Header";
import { Home } from "./pages/Home/Home";
import { Question } from "./pages/Question/Question";
import { Result } from "./pages/Result/Result";

export const App = () => {
  return (
    <Routes>
      <Route path="/" element={<Header />}>
        <Route index element={<Home />} />
        <Route path="/question/:id" element={<Question />} />
        <Route path="/result" element={<Result/>} />
      </Route>
    </Routes>
  );
};
