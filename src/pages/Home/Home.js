import { useNavigate } from "react-router-dom";
import backgroundImage from "../../assets/Background.png";
import { useDispatch } from "react-redux";
import { setQuestions } from "../../store/slices/QuestionsSlice";
import axios from "axios";
import { URL } from "../../components/Constants";
import { setLoading, stopLoading } from "../../store/slices/AnimationSlice";
import { useSelector } from "react-redux";
import { LoadingAnimation } from "../../components/LoadingAnimation";

export const Home = () => {
  const dispatch = useDispatch();

  const styles = {
    backgroundImage: `url(${backgroundImage})`,
    height: "90%",
  };
  const navigate = useNavigate();

  const isLoading = useSelector((state) => state.animation.isLoading);

  const openQuestions = () => {
    dispatch(setLoading());
    axios
      .get(`${URL}/questions`)
      .then((response) => {
        console.log(response.data.questions);
        dispatch(setQuestions(response.data.questions));
        setTimeout(() => {
          dispatch(stopLoading());
          navigate("/question/1");
        }, 1000);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div
      style={styles}
      className="flex justify-center items-center w-full bg-cover bg-center"
    >
      {!isLoading ? (
        <div className="flex flex-col justify-center items-center h-fit w-11/12 md:w-1/2 xl:w-3/5 2xl:w-1/4 text-white bg-gradient-to-b to-[#00A892] from-[#30BF8A] rounded-lg py-6 mb-0 sm:mb-16">
          <h1 className="text-3xl text-center font-headings">
            {" "}
            Welcome to the AI Career Guide!{" "}
          </h1>
          <hr className="border-green_dark border-2 w-full my-2" />
          <p className="text-lg text-center px-6 font-paragraph">
            Our application can help you to expand the horizons in finding your
            own career path! You will receive a list of vacancies suitable for
            you with a roadmap how to reach it.
          </p>
          <button
            className="text-2xl mt-8 bg-green_ligth w-2/4 py-2 rounded-lg font-headings hover:bg-green_dark"
            onClick={() => openQuestions()}
          >
            START!
          </button>
        </div>
      ) : (
        <LoadingAnimation />
      )}
    </div>
  );
};
