import backgroundImage from "../../assets/Background.png";
import { JobTitles } from "../../components/Results/JobTitles";
import { Roadmap } from "../../components/Results/Roadmap";
import { useSelector } from "react-redux";
import { Vacancies } from "../../components/Results/Vacancies";

export const Result = () => {

  const styles = {
    backgroundImage: `url(${backgroundImage})`,
    height: window.innerWidth >= 640 ? "90%" : "100%",
  };

  const isOpen = useSelector((state) => state.vacancies.isOpen);

  return (
    <div
      style={styles}
      className="flex flex-col-reverse md:flex-row justify-center md:justify-around items-center md:items-start w-full h-screen bg-cover bg-center text-white"
    >
      <Roadmap />
      {isOpen ? <Vacancies /> : <JobTitles />}
    </div>
  );
};
