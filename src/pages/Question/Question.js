import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import backgroundImage from "../../assets/Background.png";
import { useForm } from "react-hook-form";
import RightArrow from "../../assets/RightArrow.png";
import LeftArrow from "../../assets/LeftArrow.png";
import Done from "../../assets/Done.png";
import { useSelector } from "react-redux";
import { addAnswer, setJobTitles } from "../../store/slices/QuestionsSlice";
import { useDispatch } from "react-redux";
import axios from "axios";
import { URL } from "../../components/Constants";
import { LoadingAnimation } from "../../components/LoadingAnimation";
import { setLoading, stopLoading } from "../../store/slices/AnimationSlice";

export const Question = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => {};
  const [questionID, setQuestionId] = useState(
    Number(location.pathname.split("/")[2])
  );

  const questions = useSelector((state) => state.questions.questions);
  const answer = useSelector(
    (state) => state.questions.answers[questionID - 1]?.[1]
  );
  const answers = useSelector((state) => state.questions.answers);
  const dispatch = useDispatch();

  const [selectedOption, setSelectedOption] = useState(answer);
  const [canIterate, setCanIterate] = useState(true);
  const isLoading = useSelector((state) => state.animation.isLoading);

  useEffect(() => {
    setSelectedOption(answer);
  }, [answer]);

  const handleOptionChange = (e) => {
    setSelectedOption(e.target.value);
    dispatch(
      addAnswer({
        id: questionID - 1,
        key: questions[questionID - 1]["text"],
        value: e.target.value,
      })
    );
  };

  const IterateQuestion = (id, direction) => {
    if (id === questions.length + 1 && selectedOption !== undefined) {
      dispatch(setLoading());
      const data = {};
      answers.forEach((answer) => {
        data[answer[0]] = answer[1];
      });
      console.log(data);
      axios.post(`${URL}/answers`, data).then((response) => {
        console.log(response.data);
        dispatch(setJobTitles(response.data["job_titles"]));
        setTimeout(() => {
          dispatch(stopLoading());
          navigate("/result", {
            replace: true,
          });
        }, 1000);
      });
    } else {
      if (
        direction === "left" ||
        (selectedOption !== undefined && direction === "right")
      ) {
        setQuestionId(id);
        setCanIterate(true);
        navigate(`/question/${id}`, {
          replace: true,
        });
      } else {
        setCanIterate(false);
      }
    }
  };

  const styles = {
    backgroundImage: `url(${backgroundImage})`,
    height: "90%",
  };

  return (
    <div
      style={styles}
      className="flex justify-center items-center w-full bg-cover bg-center"
    >
      {!isLoading ? (
        <div className="flex flex-col justify-center items-center h-fit w-11/12 md:w-1/2 xl:w-3/5 2xl:w-1/4 text-white bg-gradient-to-b to-[#00A892] from-[#30BF8A] rounded-lg py-6 mb-0 sm:mb-16">
          <h1 className="text-3xl font-headings">
            {" "}
            Question {questionID}/{questions.length}{" "}
          </h1>
          <hr className="border-green_dark border-2 w-full my-2" />
          <p className="text-xl text-center px-6">
            {questions[questionID - 1]["text"]}
          </p>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="w-5/6 mt-4 font-paragraph"
          >
            <fieldset>
              {questions[questionID - 1]["options"].map((option, index) => {
                return (
                  <div
                    key={index}
                    className="grid grid-cols-2 gap-1 items-center w-full font-paragraph"
                    style={{ gridTemplateColumns: "32px 1fr" }}
                  >
                    <input
                      {...register(`Option ${index + 1}`)}
                      checked={selectedOption === option}
                      onChange={handleOptionChange}
                      type="radio"
                      value={option}
                      id={option}
                      className="h-max w-max hover:cursor-pointer"
                    />
                    <label
                      className="text-lg hover:cursor-pointer"
                      htmlFor={option}
                    >
                      {option}
                    </label>
                  </div>
                );
              })}
              {!canIterate ? (
                <div className="mt-2 text-red-100">Please select an option</div>
              ) : (
                <></>
              )}
            </fieldset>
            <div
              className={`flex flex-row  w-full ${
                questionID === 1 ? "justify-end" : "justify-between"
              }`}
            >
              {questionID === 1 ? (
                <></>
              ) : (
                <button
                  type="submit"
                  onClick={() => {
                    IterateQuestion(questionID - 1, "left");
                  }}
                  className="flex justify-center items-center text-2xl mt-8 bg-green_ligth w-fit px-2 rounded-lg hover:bg-green_dark"
                >
                  <img src={LeftArrow} alt="Left arrow" />
                </button>
              )}

              <button
                type="submit"
                onClick={() => {
                  IterateQuestion(questionID + 1, "right");
                }}
                className="flex justify-center items-center text-2xl mt-8 bg-green_ligth w-fit px-2 rounded-lg hover:bg-green_dark"
              >
                {questionID === questions.length ? (
                  <img src={Done} alt="Done" />
                ) : (
                  <img src={RightArrow} alt="Left arrow" />
                )}
              </button>
            </div>
          </form>
        </div>
      ) : (
        <LoadingAnimation />
      )}
    </div>
  );
};
